<?php

namespace App\Http\Controllers;

use App\Jobs\FakerDataJob;
use App\Models\User;
use Illuminate\Http\Request;

class QueueController extends Controller
{
    public function index()
    {
        $start_time = microtime(true);

        //job here
        $job = new FakerDataJob();
        $this->dispatch($job);

        //end job
        $end_time = microtime(true);
        $timediff = $end_time - $start_time;
        return "Time taken: $timediff seconds";
    }

    public function no_queue()
    {
        $start_time = microtime(true);

        //job here
        $faker = \Faker\Factory::create();
        $jumlahData = 10000;

        for ($i = 0; $i < $jumlahData; $i++) {
            $data = [
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->password,
            ];

            User::create($data);
        }
        //end job
        $end_time = microtime(true);
        $timediff = $end_time - $start_time;
        return "Time taken: $timediff seconds";
    }
}
